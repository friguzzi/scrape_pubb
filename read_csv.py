# -*- coding: utf-8 -*-

import csv
import glob

socio={}
i=1
with open('soci.csv', 'rt') as csvfile:
     reader = csv.reader(csvfile, delimiter=',', quotechar='"')
     for row in reader:
         print(str(i)+' '+row[3])
         i=i+1
         socio[row[2]+' '+row[3]]={'nome':row[2],'cognome':row[3],\
            'filenames':row[2]+' '+row[3],'aff':row[4],'consent':row[5]}
         print(len(socio))
print(len(socio))

socio.pop("First name Last name")

socio['Cristiano Castelfranchi']['filenames']="cristiano.castelfranchi@istc.cnr.it"
socio['Giovanni Maria Farinella']['filenames']="Giovanni Farinella" 
socio['Gennaro Vessio']['filenames']="Rino Vessio"
socio['Stefano Mariani']['filenames']="Stefano Mariani"
socio['Rafael Penaloza']['filenames']="Rafael Peñaloza"
socio['Giuseppina Gini']['filenames']="gini@elet.polimi.it"
socio['Andrea Galassi']['filenames']="Andrea G."
socio['Corrado Mencar']['filenames']="Mencar"
socio['Viviana Patti']['filenames']="Vivi Patti"
socio['Giosuè Lo Bosco']['filenames']="Lo Bosco"

print("soci with missing files")
for pers in socio:
    socio[pers]['files']=glob.glob("*"+socio[pers]['filenames']+"*")
for f in socio:
    #print(socio[f]['consent'])
    if not socio[f]['files']:
        print(f)

print(len(socio))
