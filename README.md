
word cloud generation https://github.com/amueller/word_cloud
pdftotext:
su mac
 brew install poppler 
oppure
 sudo port install poppler



To cluster AI*IA members
cd <folder with pdfs>
source <path to scrape_pubb>/totext.sh 
Add the csv file from the Google form to the folder and call it soci.csv
    check that the folder contains soci.csv
    check that each member has a publication
python <path to scrape_pubb>/read_csv.py 
    print people with missing files

    vectorize docs
python <path to scrape_pubb>/document_vectorization_soci.py
    generates vect.pkl e matrix.pkl

    build overall cloud
python <path to scrape_pubb>/overall_cloud.py
    generates cloud.png

    do the clustering
python <path to scrape_pubb>/document_clustering_soci.py
    generates model.pkl

    build clouds for clusters
python <path to scrape_pubb>/cluster_clouds.py
    generates cloud0.png, cloud1.png, ... cloud9.png

    compute member similarity to cluster
python <path to scrape_pubb>/classification_soci_papers.py
    generates classif-papers.pkl

    print file output.html with info on clusters
python <path to scrape_pubb>/print_clustering_soci.py
    generates output.html

    print table with member-cluster similarity
python <path to scrape_pubb>/print_soci_sim_papers_norm.py
    generates members_norm.html