:- use_module(library(http/http_open)).
:- use_module(library(sgml)).
:- use_module(library(xpath)).


/** <query>

?- scrape_no_error('http://scholar.google.it/citations?user=_wzbiu4AAAAJ&hl=en&oi=ao').
?- get_profile_url('http://scholar.google.it/scholar?hl=en&as_sdt=0%2C5&q=fabrizio+riguzzi&oq=fabrizio',PU),
   get_pub_title(PU,T).

   get_profile_url('http://scholar.google.it/scholar?hl=en&as_sdt=0%2C5&q=fabrizio+riguzzi&oq=fabrizio',PU),
   get_pub_title(PU,T),

   get_pub_pdf_url('http://www.semanticscholar.org/paper/Strategies-in-Combined-Learning-via-Logic-Programs-Lamma-Riguzzi/35d0025146e384585b30ec5236e9ec1b5af88f17',PDF).
   http_load_html_out('https://www.semanticscholar.org/paper/Strategies-in-Combined-Learning-via-Logic-Programs-Lamma-Riguzzi/35d0025146e384585b30ec5236e9ec1b5af88f17').


get_pub_pdf_url('Exploiting Inductive Logic Programming Techniques for Declarative Process Mining.',PDF).
*/

http_load_html(URL, DOM) :-
        setup_call_cleanup(http_open(URL, In,
                           [ timeout(60),status_code(_),
user_agent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36')
                           ]),
                           ( 
                                 dtd(html, DTD),
                               load_structure(stream(In),
                                              DOM,
                                              [ dtd(DTD),
                                                dialect(sgml),
                                                shorttag(false),
                                                max_errors(-1),
                                                syntax_errors(quiet)
                                              ])
                           ),
                           close(In)).

http_load_html_out(URL) :-
        setup_call_cleanup(http_open(URL, In,
                           [ timeout(60),status_code(_),
user_agent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36')
                           ]),
                           copy_stream_data(In,user_output),
                           close(In)).
get_profile_url(URL,ProfURL) :-
        catch(http_load_html(URL, DOM), Error,
              (	  print_message(warning, Error),
                  fail
              )), !,
              process_dom_prof_url(DOM,ProfURLRel),
              atomic_concat('http://scholar.google.it',ProfURLRel,ProfURL).
get_profile_url(_,_).

process_dom_prof_url(D,R):-
    xpath(D,//h4/a(@href),R).


get_pub_title(URL,Title) :-
  catch(http_load_html(URL, DOM), Error,
          (	  print_message(warning, Error),
              fail
          )), !,
          xpath(DOM,//td(@class=gsc_a_t)/a(content),[Title]).

get_pub_title(_,_).

get_pub_pdf_url(PaperURL,PDFURL):-
  catch(http_load_html(PaperURL, DOM), Error,
  (	  print_message(warning, Error),
      fail
  )), !,
  xpath(DOM,//body/script(index(last-4)),PDFURL).
%xpath(DOM,//div(@class=gs_or_ggsm)/a(@href,content('[PDF]'])),PDFURL).

get_pub_pdf_url(_,_).
