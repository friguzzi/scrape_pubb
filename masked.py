#!/usr/bin/env python
"""
Masked wordcloud
================
Using a mask you can generate wordclouds in arbitrary shapes.
"""

from os import path
from PIL import Image
import numpy as np

from wordcloud import WordCloud, STOPWORDS

d = path.dirname(__file__)

true_k=8



def gen_cloud(file_name, cloud_name):
    text = open(path.join(d, file_name)).read()

    mask = np.array(Image.open(path.join(d, "logo ai s.ombra 978x532 grey.png")))

    stopwords = set(STOPWORDS)
    stopwords.add("said")
    stopwords.add("model")
    stopwords.add("one")
    stopwords.add("et")
    stopwords.add("al")
    stopwords.add("algorithm")
    stopwords.add("also")
    stopwords.add("given")
    stopwords.add("example")
    stopwords.add("system")
    stopwords.add("user")
    stopwords.add("used")
    stopwords.add("using")
    stopwords.add("use")
    stopwords.add("method")
    stopwords.add("two")

    wc = WordCloud(background_color="white", max_words=2000, mask=mask,
                stopwords=stopwords)
    # generate word cloud
    wc.generate(text)

    # store to file
    wc.to_file(path.join(d, cloud_name))
    return

for i in range(true_k):
# Read the whole text.
    file_name="cluster"+str(i)+".txt"
    cloud_name="cloud"+str(i)+".png"
    gen_cloud(file_name,cloud_name)
# read the mask image
# taken from
# http://www.stencilry.org/stencils/movies/alice%20in%20wonderland/255fk.jpg

gen_cloud('all.txt','cloud.png')
