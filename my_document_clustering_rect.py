"""
=======================================
Clustering text documents using k-means
=======================================

This is an example showing how the scikit-learn can be used to cluster
documents by topics using a bag-of-words approach. This example uses
a scipy.sparse matrix to store the features instead of standard numpy arrays.

Two feature extraction methods can be used in this example:

  - TfidfVectorizer uses a in-memory vocabulary (a python dict) to map the most
    frequent words to features indices and hence compute a word occurrence
    frequency (sparse) matrix. The word frequencies are then reweighted using
    the Inverse Document Frequency (IDF) vector collected feature-wise over
    the corpus.

  - HashingVectorizer hashes word occurrences to a fixed dimensional space,
    possibly with collisions. The word count vectors are then normalized to
    each have l2-norm equal to one (projected to the euclidean unit-ball) which
    seems to be important for k-means to work in high dimensional space.

    HashingVectorizer does not provide IDF weighting as this is a stateless
    model (the fit method does nothing). When IDF weighting is needed it can
    be added by pipelining its output to a TfidfTransformer instance.

Two algorithms are demoed: ordinary k-means and its more scalable cousin
minibatch k-means.

Additionally, latent semantic analysis can also be used to reduce dimensionality
and discover latent patterns in the data.

It can be noted that k-means (and minibatch k-means) are very sensitive to
feature scaling and that in this case the IDF weighting helps improve the
quality of the clustering by quite a lot as measured against the "ground truth"
provided by the class label assignments of the 20 newsgroups dataset.

This improvement is not visible in the Silhouette Coefficient which is small
for both as this measure seem to suffer from the phenomenon called
"Concentration of Measure" or "Curse of Dimensionality" for high dimensional
datasets such as text data. Other measures such as V-measure and Adjusted Rand
Index are information theoretic based evaluation scores: as they are only based
on cluster assignments rather than distances, hence not affected by the curse
of dimensionality.

Note: as k-means is optimizing a non-convex objective function, it will likely
end up in a local optimum. Several runs with independent random init might be
necessary to get a good convergence.

"""

# Author: Peter Prettenhofer <peter.prettenhofer@gmail.com>
#         Lars Buitinck
# License: BSD 3 clause

from __future__ import print_function
import nltk
import string
import copy
import re
import unicodedata

from nltk.stem.porter import PorterStemmer
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer

from sklearn.cluster import KMeans, MiniBatchKMeans
import glob

import logging
from optparse import OptionParser
import sys
from time import time

import numpy as np
from os import path
from PIL import Image
import numpy as np

from wordcloud import WordCloud, STOPWORDS

from nltk.corpus import wordnet as wn
d = path.dirname(__file__)

# Display progress logs on stdout
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')

# parse commandline arguments
op = OptionParser()
op.add_option("--lsa",
              dest="n_components", type="int",
              help="Preprocess documents with latent semantic analysis.")
op.add_option("--no-minibatch",
              action="store_false", dest="minibatch", default=True,
              help="Use ordinary k-means algorithm (in batch mode).")
op.add_option("--no-idf",
              action="store_false", dest="use_idf", default=True,
              help="Disable Inverse Document Frequency feature weighting.")
op.add_option("--use-hashing",
              action="store_true", default=False,
              help="Use a hashing feature vectorizer")
op.add_option("--n-features", type=int, default=10000,
              help="Maximum number of features (dimensions)"
                   " to extract from text.")
op.add_option("--verbose",
              action="store_true", dest="verbose", default=False,
              help="Print progress reports inside k-means algorithm.")

print(__doc__)
op.print_help()


def removeNonAscii(s): return "".join(i for i in s if ord(i)<128)

def sanitize(w,inputIsUTF8=False,expungeNonAscii=False):

    map =  { 'æ': 'ae',
        'ø': 'o',
        '¨': 'o',
        'ß': 'ss',
        'Ø': 'o',
        '\xef\xac\x80': 'ff',
        '\xef\xac\x81': 'fi',
        '\xef\xac\x82': 'fl',
        u'\u2013': '-',
        'de nition': 'definition'}

  # This replaces funny chars in map
    for char, replace_char in map.items(): 
        w = re.sub(char, replace_char, w)

  #w = unicode(w, encoding='latin-1')
    #if inputIsUTF8: 
        #w = w.encode('ascii', 'ignore')

  # This gets rid of accents
    #w = ''.join((c for c in unicodedata.normalize('NFD', w) if unicodedata.category(c) != 'Mn'))

    #if expungeNonAscii: 
        #w = removeNonAscii(w)

    return w

def gen_cloud(text, cloud_name):

    mask = np.array(Image.open(path.join(d, "logo ai s.ombra 978x532 grey.png")))

    stopwords = set(STOPWORDS)
    stopwords.add("said")
    stopwords.add("model")
    stopwords.add("one")
    stopwords.add("et")
    stopwords.add("al")
    stopwords.add("algorithm")
    stopwords.add("also")
    stopwords.add("given")
    stopwords.add("example")
    stopwords.add("system")
    stopwords.add("user")
    stopwords.add("used")
    stopwords.add("using")
    stopwords.add("use")
    stopwords.add("method")
    stopwords.add("two")

    wc = WordCloud(background_color=None, mode='RGBA', collocations=False, max_words=2000, mask=mask,
                stopwords=stopwords)
    # generate word cloud
    wc.generate(text)

    # store to file
    wc.to_file(cloud_name)
    return

def gen_cloud_fre(cloud_name,centroid):



    wc = WordCloud(background_color='White', mode='RGBA', collocations=False,
    max_words=2000, width=1200, height=140)
    # generate word cloud
    wc.generate_from_frequencies(centroid)

    # store to file
    wc.to_file(cloud_name)
    return


def cluster(opts,true_k,X):

    if opts.minibatch:
        km = MiniBatchKMeans(n_clusters=true_k, init='k-means++', n_init=1,
                            init_size=1000, batch_size=1000, verbose=opts.verbose)
    else:
        km = KMeans(n_clusters=true_k, init='k-means++', max_iter=100, n_init=1,
                    verbose=opts.verbose)

    print("Clustering sparse data with %s" % km)
    t0 = time()
    km.fit(X)
    print("done in %0.3fs" % (time() - t0))
    sc=-km.score(X)
    print("score %f" %(sc))
    print()
    if not opts.use_hashing:
        print("Top terms per cluster:")

        if opts.n_components:
            original_space_centroids = svd.inverse_transform(km.cluster_centers_)
            order_centroids = original_space_centroids.argsort()[:, ::-1]
        else:
            order_centroids = km.cluster_centers_.argsort()[:, ::-1]

        terms = vectorizer.get_feature_names()
        
        for i in range(true_k):
            print("Cluster %d:" % i, end='')
            for ind in order_centroids[i, :10]:
                print(' %s' % terms[ind], end='')
            print()
    return (km,sc)

def is_interactive():
    return not hasattr(sys.modules['__main__'], '__file__')

# work-around for Jupyter notebook and IPython console
argv = [] if is_interactive() else sys.argv[1:]
(opts, args) = op.parse_args(argv)
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)

stopWords = set(stopwords.words('english'))
stopWords = stopWords.union(set(stopwords.words('italian')))
stopWords.add("et")
stopWords.add("al")
stopWords.add("wσ")
stopWords.add("riguzzi")
stopWords.add("xijk")
stopWords.add("chk")
stopWords.add("tj")

stopWords=list(stopWords)
token_dict = {}
files = glob.glob("*.txt")
for file in files:
        shakes = open(file, 'r')
        text = shakes.read()
        lowers = text.lower()
        lowers = sanitize(lowers,inputIsUTF8=False,expungeNonAscii=False)
        no_punctuation = lowers.translate(str.maketrans('','',string.punctuation))
#        no_punctuation = no_punctuation.translate(str.maketrans('','', string.printable))
        no_punctuation = ''.join(filter(lambda x: x in string.printable and not x in string.digits, no_punctuation))
#        no_punctuation = no_punctuation.translate({ord(ch): None for ch in string.digits})
        #lowers = sanitize(lowers,inputIsUTF8=True,expungeNonAscii=True)
        # nodigits = lowers.translate({ord(ch): None for ch in string.digits})
        # no_punctuation = nodigits.translate({ord(ch): None for ch in string.punctuation})


        token_dict[file] = no_punctuation


print("%d documents" % len(token_dict.values()))
print()

stemmer = PorterStemmer()
wordnet_lemmatizer = WordNetLemmatizer()
def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        if len(item) > 1:
            stemmed.append(wordnet_lemmatizer.lemmatize(item))
    
    return stemmed

def tokenize(text):
    tokens = nltk.word_tokenize(text)
    tokens=list(filter(lambda x: not x in stopWords and len(x)>1 and not wn.synsets(x, pos=wn.ADV),tokens))
    #no_punctuation=''.join(filter(lambda x: wn.synsets(x, pos=wn.ADV), ts))
    stems = stem_tokens(tokens, stemmer)
    # for i in range(len(tokens)-3):
    #     stems.append(tokens[i]+' '+tokens[i+1])
    #     stems.append(tokens[i]+' '+tokens[i+1]+' '+tokens[i+2])
    #     stems.append(tokens[i]+' '+tokens[i+1]+' '+tokens[i+2]+' '+tokens[i+3])
    # i=len(tokens)-3
    # stems.append(tokens[i]+' '+tokens[i+1])
    # stems.append(tokens[i]+' '+tokens[i+1]+' '+tokens[i+2])
    return stems

print("Extracting features from the training dataset using a sparse vectorizer")
t0 = time()
if opts.use_hashing:
    if opts.use_idf:
        # Perform an IDF normalization on the output of HashingVectorizer
        hasher = HashingVectorizer(n_features=opts.n_features,tokenizer=tokenize,
                                   stop_words=stopWords, alternate_sign=False,ngram_range=(1, 5), 
                                   norm=None, binary=False)
        vectorizer = make_pipeline(hasher, TfidfTransformer())
    else:
        vectorizer = HashingVectorizer(n_features=opts.n_features,tokenizer=tokenize,
                                       stop_words=stopWords,
                                       alternate_sign=False, norm='l2',ngram_range=(1, 5), 
                                       binary=False)
else:
    vectorizer = TfidfVectorizer(max_df=0.5, max_features=opts.n_features,tokenizer=tokenize,
                                 min_df=2, stop_words=stopWords,ngram_range=(1, 5), 
                                 use_idf=opts.use_idf)
#X = vectorizer.fit_transform(token_dict.values())

print("done in %fs" % (time() - t0))
#print("n_samples: %d, n_features: %d" % X.shape)
print()

if opts.n_components:
    print("Performing dimensionality reduction using LSA")
    t0 = time()
    # Vectorizer results are normalized, which makes KMeans behave as
    # spherical k-means for better results. Since LSA/SVD results are
    # not normalized, we have to redo the normalization.
    svd = TruncatedSVD(opts.n_components)
    normalizer = Normalizer(copy=False)
    lsa = make_pipeline(svd, normalizer)

    X = lsa.fit_transform(X)

    print("done in %fs" % (time() - t0))

    explained_variance = svd.explained_variance_ratio_.sum()
    print("Explained variance of the SVD step: {}%".format(
        int(explained_variance * 100)))

    print()

k=8
# #############################################################################
# Do the actual clustering

true_k = 0
best_km = None
best_sc = -1e10
''' n_iter=10
for k in [2,3,4]:
    print("N cluster %d" % k)
    for iter in range(n_iter):
        print("Iter %d" % iter)
        (km_k, sc) = cluster(opts, k, X)
        if sc > best_sc:
            true_k = k
            best_km = copy.deepcopy(km_k)

km = best_km

if not opts.use_hashing:
    print("Top terms per cluster:")

    if opts.n_components:
        original_space_centroids = svd.inverse_transform(km.cluster_centers_)
        order_centroids = original_space_centroids.argsort()[:, ::-1]
    else:
        order_centroids = km.cluster_centers_.argsort()[:, ::-1]

    terms = vectorizer.get_feature_names()
    c=km.predict(X)
    card = [0 for i in range(X.shape[0])]
    for i in range(X.shape[0]):
        card[c[i]]=card[c[i]]+1


    for i in range(true_k):
        print("Cluster %d, card %d:" % (i,card[i]), end='')
        for ind in order_centroids[i, :10]:
            print(' %s' % terms[ind], end='')
        fre={}
        for ind in order_centroids[i]:
            fre[terms[ind]]=km.cluster_centers_[i,ind]        
        cloud_name="./cloud"+str(i)+".png"
        gen_cloud_fre(cloud_name,fre)
        print() '''
    
    # # A list of the keys of dictionary.
    # list_keys = [ k for k in token_dict ]
    # # or a list of the values.
    # list_values = [ v for v in token_dict.values() ]
    # c=km.predict(X)
    

    # cluster_text={}
    # for i in range(true_k):
    #     cluster_text[i]=""

    # for i in range(len(list_values)):
    #     cluster_text[c[i]]=' '.join([cluster_text[c[i]],list_values[i]])
    
    # for i in range(true_k):
    #     file_name="cluster"+str(i)+".txt"
    #     shakes = open(file_name, 'w')
    #     text = shakes.write(cluster_text[i])
    #     shakes.close()


#terms = vectorizer.get_feature_names()
f = open('all.txt', 'r')
text = f.read()
f.close()
lowers = text.lower()
lowers = sanitize(lowers,inputIsUTF8=False,expungeNonAscii=False)
# pos=lowers.find(' nition')-5
# posu=pos+11
# print((pos,posu))
# print(lowers[pos:posu])
no_punctuation = lowers.translate(str.maketrans('','',string.punctuation))
no_punctuation = ''.join(filter(lambda x: x in string.printable and not x in string.digits, no_punctuation))

print(len(no_punctuation))

cv=CountVectorizer(
        lowercase=True, 
        ngram_range=(1, 5), 
        strip_accents=None, max_features=10000,
        tokenizer=tokenize,min_df=0,
                                       stop_words=stopWords
                            )
X = cv.fit_transform([no_punctuation])
#ts=tokenize(no_punctuation)

terms = cv.get_feature_names()
#print(len(ts))
t0 = time()

print("done in %fs" % (time() - t0))
mat=[no_punctuation]
fre_mat=cv.transform([no_punctuation])
#fre_mat=vectorizer.transform(mat)
fre_mat=fre_mat.todense()
fre_mat.argsort()[:, ::-1]
#for ind in fre_mat[0, :100]:
 #   print(' %s' % terms[ind], end='')


fre={}
for ind in range(fre_mat.shape[1]):
    fre[terms[ind]]=fre_mat[0,ind]   

srti = [(k, fre[k]) for k in sorted(fre, key=fre.get, reverse=True)]
for i in range(0,100):
    print(srti[i])

gen_cloud_fre('cloud.png',fre)

#gen_cloud(lowers,'cloudnof.png')



