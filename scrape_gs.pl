:- use_module(library(http/http_open)).
:- use_module(library(sgml)).
:- use_module(library(xpath)).


/** <query>

?- scrape_no_error('http://scholar.google.it/citations?user=_wzbiu4AAAAJ&hl=en&oi=ao').
?- get_profile_url('http://scholar.google.it/scholar?hl=en&as_sdt=0%2C5&q=fabrizio+riguzzi&oq=fabrizio',PU),
   get_pub_title(PU,T).

   get_profile_url('http://scholar.google.it/scholar?hl=en&as_sdt=0%2C5&q=fabrizio+riguzzi&oq=fabrizio',PU),
   get_pub_title(PU,T),
   get_pub_pdf_url(T,PDF).

get_pub_pdf_url('Exploiting Inductive Logic Programming Techniques for Declarative Process Mining.',PDF).
*/

http_load_html(URL, DOM) :-
        setup_call_cleanup(http_open(URL, In,
                           [ timeout(60),
user_agent('Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.8) Gecko/20100225 Iceweasel/3.5.8 (like Firefox/3.5.8)')
                           ]),
                           (   dtd(html, DTD),
                               load_structure(stream(In),
                                              DOM,
                                              [ dtd(DTD),
                                                dialect(sgml),
                                                shorttag(false),
                                                max_errors(-1),
                                                syntax_errors(quiet)
                                              ])
                           ),
                           close(In)).
get_profile_url(URL,ProfURL) :-
        catch(http_load_html(URL, DOM), Error,
              (	  print_message(warning, Error),
                  fail
              )), !,
              process_dom_prof_url(DOM,ProfURLRel),
              atomic_concat('http://scholar.google.it',ProfURLRel,ProfURL).
get_profile_url(_,_).

process_dom_prof_url(D,R):-
    xpath(D,//h4/a(@href),R).


get_pub_title(URL,Title) :-
  catch(http_load_html(URL, DOM), Error,
          (	  print_message(warning, Error),
              fail
          )), !,
          xpath(DOM,//td(@class=gsc_a_t)/a(content),[Title]).

get_pub_title(_,_).

get_pub_pdf_url(Title,PDFURL):-
  atom_string(Title,TitleS),
  split_string(TitleS," ",".",Words),
  atomics_to_string(Words,'+',TitlePlus),
  atomics_to_string(['http://scholar.google.it/scholar?hl=en&as_sdt=0%2C5&q=',
  TitlePlus,'&btnG='],SearchURL),
  catch(http_load_html(SearchURL, DOM), Error,
  (	  print_message(warning, Error),
      fail
  )), !,
  xpath(DOM,//div(@class=gs_or_ggsm)/a,Anch),
  xpath(Anch,/span(content),['[PDF]']),
  xpath(Anch,/self(@href),PDFURL).
%xpath(DOM,//div(@class=gs_or_ggsm)/a(@href,content('[PDF]'])),PDFURL).

get_pub_pdf_url(_,_).