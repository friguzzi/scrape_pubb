:- use_module(library(http/http_open)).
:- use_module(library(sgml)).
:- use_module(library(xpath)).
:- use_module(library(csv)).

/** <query>

   get_pub_doi_url('http://dblp.org/pers/hd/r/Riguzzi:Fabrizio',T),
   get_pub_pdf_url(T,PDF).



get_pub_pdf_url('Exploiting Inductive Logic Programming Techniques for Declarative Process Mining.',PDF).

get_scihub_pdr_url('http://sci-hub.bz/https://doi.org/10.1016/j.ijar.2016.10.002',D),
save_pdf(D,'ciao.pdf').
save_pdf('http://ds.ing.unife.it/~friguzzi/Papers/NguRig17-IMAKE-BC.pdf','im.pdf').
*/

main:-
  num_pap(N),
  main_int(N).

main_int(N):-
  log_file(LF),
  main(LF,N).

main(LF,N):-
  open(LF,write,SL),
  assert(log_stream(SL)),
  findnsols(N, _, get_pub('http://mcs.unife.it/~friguzzi/publications_year.htm'), _ListURLPubb),
  close(SL).

    
log_file('../log.log').
num_pap(10000).


log(A):-
  log_stream(S),
  writeln(S,A),
  writeln(user_output,A).

http_load_html(URL, DOM) :-
        setup_call_cleanup(http_open(URL, In,
                           [ timeout(60),status_code(_),
user_agent('Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.8) Gecko/20100225 Iceweasel/3.5.8 (like Firefox/3.5.8)')
                           ]),
                               load_html(stream(In),DOM,[]),
                           close(In)).



get_pub(URL) :-
  catch(http_load_html(URL, DOM), Error,
          (	  print_message(warning, Error),
              log(page_non_existent(URL))
          )), !,
          xpath(DOM,//a,Cont),
          get_ind_pub(Cont).

get_ind_pub(DOM):-
          xpath(DOM,/self(content=['.pdf']),Cont),
          xpath(Cont,/self(@href),DOI_URL),
          log(pub_url(DOI_URL)),
          get_pub_url(DOI_URL).

get_ind_pub(DOM):-
          xpath(DOM,/self(contains(@href,'http://arxiv.org/') ) ,Cont),
          xpath(Cont,/self(@href),DOI_URL),
          log(arxiv_url(DOI_URL)),
          get_pub_url(DOI_URL).

   %     xpath(DOM,//li(@class='entry article')//a(content),Title).
    %          xpath(DOM,//span(@class=title,content),[Title]).

get_pub_pdf_url(DOI_URL,PDFURL):-
  doi_url(DOI_URL),!,fail,
  atom_string(DOI_URL,DOI_URLS),
  atomics_to_string(['http://sci-hub.bz/',
        DOI_URLS],PaperURL),
    log(trying(PaperURL)),
   get_scihub_pdr_url(PaperURL,PDFURL),
   log(pdfurl(PDFURL)).

get_pub_pdf_url(DOI_URL,PDFURL):-
  arxiv_url(DOI_URL),
  atom_concat('http://arxiv.org/abs/',ID,DOI_URL),!,
  atomic_list_concat(['http://arxiv.org/pdf/',ID,'.pdf'],PDFURL),
  log(pdfurl(PDFURL)).

get_pub_pdf_url(DOI_URL,PDFURL):-
  arxiv_url(DOI_URL),
  atom_concat('http://arxiv.org/pdf/',ID,DOI_URL),!,
  atomic_list_concat(['http://arxiv.org/pdf/',ID,'.pdf'],PDFURL),
  log(pdfurl(PDFURL)).

get_pub_pdf_url(URL,PDFURL):-
  aaai_url(URL),!,
  log(aaai_url(URL)),
  catch(http_load_html(URL, DOM), Error,
  (	  print_message(warning, Error),
      log(absent_page(URL)),!,
      fail
  )),
  xpath(DOM,//frame(@src),ViewURL),
  log(view_url(ViewURL)),
  catch(http_load_html(ViewURL, DOMV), ErrorV,
  (	  print_message(warning, ErrorV),
      log(absent_pagev(ViewURL)),!,
      fail
  )),
  xpath(DOMV,//div(@id='paper')/a(@href),NURL),
  catch(http_load_html(NURL, DOMN), ErrorN,
  (	  print_message(warning, ErrorN),
      log(absent_pagen(NURL)),!,
      fail
  )),
  xpath(DOMN,//frame(@src),IURL),
  catch(http_load_html(IURL, DOMI), ErrorI,
  (	  print_message(warning, ErrorI),
      log(absent_pagei(IURL)),!,
      fail
  )),
  xpath(DOMI,//p/a(index(3)),A),
  xpath(A,/self(@href),PDFURL),
  log(pdfurl(PDFURL)).

get_pub_pdf_url(URL,PDFURL):-
  ijcai_url(URL),!,
  log(ijcai_url(URL)),
  catch(http_load_html(URL, DOM), Error,
  (	  print_message(warning, Error),
      log(absent_page(URL)),!,
      fail
  )),
  xpath(DOM,//p/a(@href),PURL),
  atom_concat('http://www.ijcai.org',PURL,PDFURL),
  log(pdfurl(PDFURL)).

get_pub_pdf_url(PDFURL,PDFURL):-
   log(pdfurl(PDFURL)).

ijcai_url(URL):-
  atom_concat('http://www.ijcai.org/',_,URL).

aaai_url(URL):-
  atom_concat('http://aaai.org',_,URL),!.

aaai_url(URL):-
  atom_concat('https://aaai.org',_,URL).

arxiv_url(URL):-
  atom_concat('http://arxiv.org/',_,URL).

doi_url(DOI_URL):-
  atom_concat('https://doi.org/',_,DOI_URL),!.

doi_url(DOI_URL):-
  atom_concat('http://doi.acm.org/',_,DOI_URL),!.
doi_url(DOI_URL):-
  atom_concat('https://doi.ieeecomputersociety.org/',_,DOI_URL).

get_scihub_pdr_url(PaperURL,PDFURL):-
  catch(http_load_html(PaperURL, DOM), Error,
  (	  print_message(warning, Error),
      log(absent_page(PaperURL)),!,
      fail
  )),
  xpath(DOM,//iframe(@src),PDFURL),
  log(scipubpdfurl(PDFURL)),!.
%xpath(DOM,//div(@class=gs_or_ggsm)/a(@href,content('[PDF]'])),PDFURL).



get_pub_url(URL):-
  get_pub_pdf_url(URL,URLPDF),
  url_to_file(URLPDF,File),
  (exists_file(File)->
    log(file_exists(File))
  ;
    log(downloading(URLPDF)),
    save_pdf(URLPDF,File),
    log(downloaded(URLPDF))
  ).

url_to_file(U,F):-
  atom_chars(U,S),
  append(_,['.','p','d','f'],S),!,
  maplist(remove_forbidden,S,S1),
  atom_chars(F,S1).

url_to_file(U,F):-
  aaai_url(U),
  atom_chars(U,S),
  append(S,['.','p','d','f'],S1),!,
  maplist(remove_forbidden,S1,S2),
  atom_chars(F,S2).

save_pdf(URL,File):-
    setup_call_cleanup(http_open(URL, In,
            [ timeout(60),status_code(_),final_url(Final),
user_agent('Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.8) Gecko/20100225 Iceweasel/3.5.8 (like Firefox/3.5.8)')
            ]),
(open(File,write,F, [type(binary)]),
log(final(Final)),
copy_stream_data(In,F),close(F)),
  close(In)).


remove_forbidden('/','_'):-!.

remove_forbidden('%','_'):-!.

remove_forbidden(':','_'):-!.

remove_forbidden(C,C).

